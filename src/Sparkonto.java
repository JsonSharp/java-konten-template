/**
 * Created by john on 01.12.15.
 */
public class Sparkonto extends Konto {

    public float Zinssatz;

    public Sparkonto(int kontonummer, float kontostand, float zinssatz) {
        //super() fuehrt den konstructor der parent klasse Konto aus
        super(kontonummer, kontostand);
        Zinssatz = zinssatz;
    }
}
