/**
 * Created by john on 01.12.15.
 */
public class Festgeldkonto extends Konto {
    public int Zeitrahmen;


    public Festgeldkonto(int kontonummer, float kontostand, int zeitrahmen) {
        //super() fuehrt den konstructor der parent klasse Konto aus
        super(kontonummer, kontostand);
        Zeitrahmen = zeitrahmen;
    }
}
