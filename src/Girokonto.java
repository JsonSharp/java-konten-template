/**
 * Created by john on 01.12.15.
 */
public class Girokonto extends Konto {
    public float Dispozinsatz;
    public float Disporahmen;

    public Girokonto(int kontonummer, float kontostand,float dispozinsatz, float disporahmen) {
        //super() fuehrt den konstructor der parent klasse Konto aus
        super(kontonummer, kontostand);
        Dispozinsatz = dispozinsatz;
        Disporahmen = disporahmen;
    }

}
