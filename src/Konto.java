/**
 * Created by john on 01.12.15.
 */
public class Konto {
    public int Kontonummer;
    public float Kontostand;

    public Konto(int kontonummer, float kontostand) {
        //temporaerer string um die laenge der Kontonummer zu checken
        String kontonummerCheck = kontonummer+"";
        //wenn die kontonummer 6 stellen hat werden die werte uebergeben, wenn nicht werden beide auf 0 gesetzt
        if (kontonummerCheck.length() == 6) {
            Kontonummer = kontonummer;
            Kontostand  = kontostand;
        } else {
            Kontonummer = 0;
            Kontostand = 0;
        }




    }
}
